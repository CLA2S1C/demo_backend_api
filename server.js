const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname + "/uploaded"));
app.use(cors());

app.use("/api/v2/authen/", require("./routes/api_authen"));
app.use("/api/v2/stock/", require("./routes/api_stock"));
app.use("/api/v2/rent/", require("./routes/api_rent"));

app.listen("8085", () => {
  console.log("Backend is running..");
});
