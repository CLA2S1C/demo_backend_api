const Sequelize = require("sequelize");
const sequelize = require("../common/db_instance");

const bill = sequelize.define(
  "bill",
  {
    BillId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    BillName: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    RentId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    BillBank: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    BillDate: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    BillPrice: {
      type: Sequelize.NUMBER,
      allowNull: false,
    },
    BillImages: {
      type: Sequelize.STRING,
    },
  },
  {
    // option
  }
);
(async () => {
  await bill.sync({ force: false });
})();

module.exports = bill;
