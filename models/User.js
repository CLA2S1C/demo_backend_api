const Sequelize = require("sequelize");
const sequelize = require("../common/db_instance");

const user = sequelize.define(
  "user",
  {
    UserId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    UserMail: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    UserPassword: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    UserFullName: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    UserRole: {
      type: Sequelize.STRING,
      defaultValue: "normal",
      allowNull: false,
    },
  },
  {
    // option
  }
);
(async () => {
  await user.sync({ force: false });
})();

module.exports = user;
