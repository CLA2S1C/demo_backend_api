const Sequelize = require("sequelize");
const sequelize = require("../common/db_instance");

const rentlog = sequelize.define(
  "rentlog",
  {
    RentlogId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    RentId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    ProductId: {
      type: Sequelize.INTEGER,
    },
    RentPrice: {
      type: Sequelize.NUMBER,
    },
    RentNum: {
      type: Sequelize.NUMBER,
    },
  },
  {
    // option
  }
);
(async () => {
  await rentlog.sync({ force: false });
})();

module.exports = rentlog;
