const Sequelize = require("sequelize");
const sequelize = require("../common/db_instance");

const product = sequelize.define(
  "product",
  {
    ProductId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    ProductName: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    ProductPeople: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    ProductRain: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
    },
    ProductWidth: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    ProductHight: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    ProductDetail: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    ProductInStock: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    ProductInRent: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    ProductPrice: {
      type: Sequelize.NUMBER,
      allowNull: false,
    },
    ProductDiscount: {
      type: Sequelize.NUMBER,
      defaultValue: 0,
    },
    ProductImages: {
      type: Sequelize.TEXT,
    },
    focusItem: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  },
  {
    // option
  }
);
(async () => {
  await product.sync({ force: false });
})();

module.exports = product;
