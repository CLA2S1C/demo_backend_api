const Sequelize = require("sequelize");
const sequelize = require("../common/db_instance");

const product = sequelize.define(
  "imageproduct",
  {
    ImageProductId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    ProductId: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    ImageProductName: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  },
  {
    // option
  }
);
(async () => {
  await product.sync({ force: false });
})();

module.exports = product;
