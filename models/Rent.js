const Sequelize = require("sequelize");
const sequelize = require("../common/db_instance");

const rent = sequelize.define(
  "rent",
  {
    RentId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    RentStatus: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    //1 = รอชำระเงิน
    //2 = กำลังเตรียมสินค้า
    //3 = จัดส่งสินค้าเรียบร้อย
    //4 = เก็บสินค้าคืน
    RentPriceTotal: {
      type: Sequelize.NUMBER,
      allowNull: false,
    },
    RentPriceProductTotal: {
      type: Sequelize.NUMBER,
      allowNull: false,
    },
    Rent_Date_Start: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Rent_Date_End: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Rent_Shipping_Cost: {
      type: Sequelize.NUMBER,
      allowNull: false,
    },
    UserId: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Rent_Address_Name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Rent_Address_Phone: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Rent_Address_Post: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Rent_Address_District: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Rent_Address_Detail: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    Rent_Address_Province: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  },
  {
    // option
  }
);
(async () => {
  await rent.sync({ force: false });
})();

module.exports = rent;
