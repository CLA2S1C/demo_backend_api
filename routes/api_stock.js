const express = require("express");
const router = express.Router();
const constants = require("../common/constant");

const formidable = require("formidable");
const path = require("path");
const fs = require("fs-extra");
const imageproduct = require("../models/ImageProduct");
const product = require("../models/Product");
const sequelize = require("sequelize");
const Op = sequelize.Op;

// getProduct
router.get("/getallproduct", async (req, res) => {
  let result = await product.findAll({
    order: sequelize.literal("ProductId DESC"),
  });
  res.json(result);
});

// getImagesProduct
router.get("/getimageproduct", async (req, res) => {
  let result = await imageproduct.findAll();
  res.json(result);
});

// getOneImagesProduct
router.post("/getoneimageproduct", async (req, res) => {
  let result = await imageproduct.findOne({
    where: { ProductId: req.body.ProductId },
  });
  res.json(result);
});

// getManyImagesProduct
router.post("/getmanyimageproduct", async (req, res) => {
  let result = await imageproduct.findAll({
    where: { ProductId: req.body.id },
  });
  res.json(result);
});

const uploadImage = async (files, doc, number) => {
  if (files != null) {
    var fileExtention = files.name.split(".")[1];
    let fileName = `${doc}-${number}.${fileExtention}`;
    var newpath =
      path.resolve(__dirname + "/uploaded/images_product/") + "/" + fileName;
    if (fs.exists(newpath)) {
      await fs.remove(newpath);
    }
    await fs.moveSync(files.path, newpath);

    //update image Database
    let imageProductAdd = await imageproduct.create({
      ProductId: doc,
      ImageProductName: fileName,
    });

    return imageProductAdd;
  }
};

const uploadImage2 = async (files, doc, number) => {
  if (files != null) {
    var fileExtention = files.ProductImages.name.split(".")[1];
    let fileName = `${doc}-${number}.${fileExtention}`;
    var newpath =
      path.resolve(__dirname + "/uploaded/images_product/") + "/" + fileName;
    if (fs.exists(newpath)) {
      await fs.remove(newpath);
    }
    await fs.moveSync(files.ProductImages.path, newpath);

    //update image Database
    let imageProductAdd = await imageproduct.update(
      {
        ImageProductName: fileName,
      },
      { where: { ProductId: doc } }
    );

    return imageProductAdd;
  }
};
// updateProduct
router.put("/product", async (req, res) => {
  try {
    const from = new formidable.IncomingForm();
    from.parse(req, async (error, fields, files) => {
      let result = await product.update(fields, {
        where: { ProductId: parseInt(fields.id) },
      });
      // result = await uploadImage2(files, fields.ProductId, 1);
      res.json({
        result: constants.kResultOk,
        message: JSON.stringify(result),
      });
    });
  } catch (error) {
    res.json({
      result: constants.kResultNok,
      message: JSON.stringify(error),
    });
  }
});
// addProduct
router.post("/createproduct", async (req, res) => {
  try {
    var from = new formidable.IncomingForm();
    let i = 1;
    let images = [];
    from
      .on("file", async (field, file) => {
        await images.push(file);
      })
      .parse(req, async (error, fields, file) => {
        let result = await product.create(fields);
        await images.forEach(async (data) => {
          await uploadImage(data, result.ProductId, i++);
        });
        res.json({
          result: constants.kResultOk,
          message: JSON.stringify(result),
        });
      });
  } catch (error) {
    res.json({
      result: constants.kResultNok,
      message: JSON.stringify(error),
    });
  }
});

// deleteProduct
router.delete("/product/:id", async (req, res) => {
  try {
    const { id } = req.params;
    let result = await imageproduct.findAll({ where: { ProductId: id } });
    if (result !== null) {
      let json = await JSON.parse(JSON.stringify(result));
      await json.forEach(async (value) => {
        await fs.remove(
          path.resolve(__dirname + "/uploaded/images_product/") +
            "/" +
            value.images_product
        );
      });
    }
    result = await product.destroy({ where: { ProductId: id } });
    res.json({ result: constants.kResultOk, message: result });
  } catch (error) {
    res.json({ result: constants.kResultNok, message: "Error" });
  }
});

// getProductByid
router.get("/product/:id", async (req, res) => {
  try {
    let result = await product.findOne({
      where: { ProductId: req.params.id },
    });
    if (result !== null) {
      res.json(result);
    } else {
      res.json({ message: "error" });
    }
  } catch (error) {
    res.json({ message: "error" });
  }
});

// getProductByKeyword
router.get("/product/keyword/:keyword", async (req, res) => {
  const { keyword } = req.params;
  let result = await product.findAll({
    where: { ProductName: { [Op.like]: `%${keyword}%` } },
  });
  res.json(result);
});

module.exports = router;
