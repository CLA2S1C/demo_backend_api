const express = require("express");
const router = express.Router();
const user = require("../models/User");
const bcrypt = require("bcryptjs");
const constants = require("../common/constant");

router.post("/login", async (req, res) => {
  const { UserMail, UserPassword } = req.body;
  console.log(UserMail, UserPassword);
  let result = await user.findOne({ where: { UserMail: UserMail } });
  if (result != null) {
    if (bcrypt.compareSync(UserPassword, result.UserPassword)) {
      res.json({
        result: constants.kResultOk,
        message: JSON.stringify(result),
      });
    } else {
      res.json({ result: constants.kResultNok, message: "Incorrect password" });
    }
  } else {
    res.json({ result: constants.kResultNok, message: "Incorrect username" });
  }
});

router.post("/register", async (req, res) => {
  try {
    let chkMail = await user.findOne({
      where: { UserMail: req.body.UserMail },
    });
    console.log(chkMail);
    if (chkMail != null) {
      throw "มีอีเมลนั้อยู่ในระบบแล้ว";
    }
    req.body.UserPassword = bcrypt.hashSync(req.body.UserPassword, 8);
    let result = await user.create(req.body);
    res.json({ result: constants.kResultOk, message: JSON.stringify(result) });
  } catch (error) {
    res.json({ result: constants.kResultNok, message: JSON.stringify(error) });
  }
});

router.post("/info", async (req, res) => {
  try {
    const { UserId } = req.body;
    console.log(UserId);
    let result = await user.findOne({
      where: { UserId: UserId },
    });
    res.json({ result: constants.kResultOk, message: JSON.stringify(result) });
  } catch (error) {
    res.json({ result: constants.kResultNok, message: JSON.stringify(error) });
  }
});

module.exports = router;
