const express = require("express");
const router = express.Router();
const constants = require("../common/constant");

const formidable = require("formidable");
const path = require("path");
const fs = require("fs-extra");

const rent = require("../models/Rent");
const rentlog = require("../models/RentLog");
const bill = require("../models/Bill");
const product = require("../models/Product");

router.post("/createrent", async (req, res) => {
  try {
    const {
      RentPriceTotal,
      Rent_Address_Name,
      Rent_Address_Phone,
      Rent_Date_Start,
      Rent_Date_End,
      Rent_Shipping_Cost,
      UserId,
      Rent_Address_Post,
      Rent_Address_District,
      Rent_Address_Detail,
      Rent_Address_Province,
      RentPriceProductTotal,
      LogRent,
    } = req.body;
    let result = await rent.create({
      RentStatus: 0,
      Rent_Address_Name: Rent_Address_Name,
      Rent_Address_Phone: Rent_Address_Phone,
      RentPriceTotal: RentPriceTotal,
      Rent_Date_Start: Rent_Date_Start,
      Rent_Date_End: Rent_Date_End,
      Rent_Shipping_Cost: Rent_Shipping_Cost,
      RentPriceProductTotal: RentPriceProductTotal,
      UserId: UserId,
      Rent_Address_Post: Rent_Address_Post,
      Rent_Address_District: Rent_Address_District,
      Rent_Address_Detail: Rent_Address_Detail,
      Rent_Address_Province: Rent_Address_Province,
    });
    for (var key in LogRent) {
      if (LogRent.hasOwnProperty(key)) {
        await rentlog.create({
          RentId: result.RentId,
          ProductId: LogRent[key].ProductId,
          RentPrice: LogRent[key].RentPrice,
          RentNum: LogRent[key].RentNum,
        });
        let pro = await product.findOne({
          where: { ProductId: LogRent[key].ProductId },
        });
        let json_pro = JSON.parse(JSON.stringify(pro));
        await product.update(
          {
            ProductInStock: json_pro.ProductInStock - LogRent[key].RentNum,
            ProductInRent: json_pro.ProductInRent + LogRent[key].RentNum,
          },
          {
            where: { ProductId: LogRent[key].ProductId },
          }
        );
      }
    }
    res.json({
      result: constants.kResultOk,
      message: JSON.stringify(result),
    });
  } catch (error) {
    res.json({
      result: constants.kResultNok,
      message: JSON.stringify(error),
    });
  }
});
router.post("/getrentbyid", async (req, res) => {
  try {
    let result = await rentlog.findAll({ where: { RentId: req.body.RentId } });
    res.json({
      result: constants.kResultOk,
      message: JSON.stringify(result),
    });
  } catch (error) {
    res.json({
      result: constants.kResultNok,
      message: JSON.stringify(error),
    });
  }
});
router.post("/getrentbyuser", async (req, res) => {
  try {
    let result = await rent.findAll({ where: { UserId: req.body.UserId } });
    res.json({
      result: constants.kResultOk,
      message: JSON.stringify(result),
    });
  } catch (error) {
    res.json({
      result: constants.kResultNok,
      message: JSON.stringify(error),
    });
  }
});
router.post("/getrent", async (req, res) => {
  try {
    let result = await rent.findAll();
    res.json({
      result: constants.kResultOk,
      message: JSON.stringify(result),
    });
  } catch (error) {
    res.json({
      result: constants.kResultNok,
      message: JSON.stringify(error),
    });
  }
});

router.post("/getbill", async (req, res) => {
  try {
    let result = await bill.findOne({
      where: { RentId: parseInt(req.body.RentId) },
    });
    res.json({
      result: constants.kResultOk,
      message: JSON.stringify(result),
    });
  } catch (error) {
    res.json({
      result: constants.kResultNok,
      message: JSON.stringify(error),
    });
  }
});
router.post("/getonerent", async (req, res) => {
  try {
    let result = await rent.findOne({ where: parseInt(req.body.RentId) });
    res.json({
      result: constants.kResultOk,
      message: JSON.stringify(result),
    });
  } catch (error) {
    res.json({
      result: constants.kResultNok,
      message: JSON.stringify(error),
    });
  }
});
const uploadImage = async (files, doc) => {
  if (files.BillImages != null) {
    var fileExtention = files.BillImages.name.split(".")[1];
    doc.BillImages = `${doc.BillId}.${fileExtention}`;
    var newpath =
      path.resolve(__dirname + "/uploaded/images_bill/") + "/" + doc.BillImages;
    if (fs.exists(newpath)) {
      await fs.remove(newpath);
    }
    await fs.moveSync(files.BillImages.path, newpath);

    //update image Database
    let result = await bill.update(
      { BillImages: doc.BillImages },
      { where: { BillId: doc.BillId } }
    );
    return result;
  }
};

router.post("/payment", async (req, res) => {
  try {
    let findRent = await rent.findOne({
      where: { RentId: parseInt(req.body.RentId) },
    });
    if (findRent !== null && findRent.RentStatus === 0) {
      let result = await bill.create(req.body);
      await rent.update(
        {
          RentStatus: 1,
        },
        { where: { RentId: parseInt(req.body.RentId) } }
      );
      res.json({
        result: constants.kResultOk,
        message: JSON.stringify(result),
      });
    } else if (findRent === null) {
      res.json({
        result: constants.kResultNok,
        message: "ไม่พบรายการเช่านี้",
      });
    } else {
      res.json({
        result: constants.kResultNok,
        message: "รายการนี้ ถูกชำระเงินเรียบร้อยแล้ว",
      });
    }
    // result = await uploadImage(files, result);
  } catch (error) {
    res.json({ result: constants.kResultNok, message: JSON.stringify(error) });
  }
});

router.post("/updatestatus", async (req, res) => {
  try {
    let result = await rent.update(
      {
        RentStatus: req.body.RentStatus,
      },
      { where: { RentId: req.body.RentId } }
    );
    if (req.body.RentStatus === 3) {
      let result = await rentlog.findAll({
        where: { RentId: req.body.RentId },
      });
      let json = JSON.parse(JSON.stringify(result));
      for (const x of json) {
        let products = await product.findOne({
          where: { ProductId: x.ProductId },
        });
        let jsonProduct = JSON.parse(JSON.stringify(products));
        await product.update(
          {
            ProductInStock: jsonProduct.ProductInStock + x.RentNum,
            ProductInRent: jsonProduct.ProductInRent - x.RentNum,
          },
          { where: { ProductId: x.ProductId } }
        );
      }
    }
    res.json({
      result: constants.kResultOk,
      message: JSON.stringify(result),
    });
  } catch (error) {
    res.json({ result: constants.kResultNok, message: JSON.stringify(error) });
  }
});

router.post("/delrent", async (req, res) => {
  try {
    let result = await rentlog.findAll({
      where: { RentId: req.body.RentId },
    });
    let json = JSON.parse(JSON.stringify(result));
    for (const x of json) {
      let products = await product.findOne({
        where: { ProductId: x.ProductId },
      });
      let jsonProduct = JSON.parse(JSON.stringify(products));
      await product.update(
        {
          ProductInStock: jsonProduct.ProductInStock + x.RentNum,
          ProductInRent: jsonProduct.ProductInRent - x.RentNum,
        },
        { where: { ProductId: x.ProductId } }
      );
    }
    let deleteRent = await rent.destroy({ where: { RentId: req.body.RentId } });
    await rentlog.destroy({
      where: { RentId: req.body.RentId },
    });
    res.json({
      result: constants.kResultOk,
      message: JSON.stringify(deleteRent),
    });
  } catch (error) {
    res.json({
      result: constants.kResultNok,
      message: JSON.stringify(deleteRent),
    });
  }
});

module.exports = router;
